@extends('layouts.layouts_v')
@section('title', 'Student Login')
@section('brand', 'Please login using your student code')
@section('cover')
<img src = "{{ url('/images/logos/spc.png') }}" class="image-responsive nav-img">
<img src = "{{ url('/images/logos/comelec.png') }}" class="image-responsive nav-img">
<br>
<h1>San Pedro College Commission on Elections</h1>
<br><br>

@endsection

@section('content')
	{!! Form::open(['route' => ['votes.ValidateStudent', $events->id], 'Method' => 'POST']) !!}
	{!! Form::label('Student Code') !!}
	{!! Form::text('student_code', null, ['class' => 'form-control input-lg', 'required' => '', 'placeholder' => 'Input student code...']) !!} 
	{!! Form::hidden('event_id', $events->id) !!}
	{!! Form::submit('Submit', ['class' => 'btn btn-primary', 'style' => 'margin: 20px']) !!}
	{!! Form::close() !!}

	@if(Session::has('message'))
		<div class = "alert alert-danger">
			{{ Session::get('message') }}
		</div>
	@endif
@endsection