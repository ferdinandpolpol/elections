<style>
.row{
	border-radius: 5px;
	background-color: #44487e;
	box-shadow: 1px 3px #000;
	padding: 10px 0 10px 0;
}
.img-circle{
	border: 5px solid #333;
}

.highlight-row{
	background-color: #fff;
	transition: background-color 0.5s ease
}
</style>


@extends('layouts.layouts_v')

@section('title', 'Please vote wisely!')

@section('cover', 'WELCOME')

@section('content')
<h4>{{ $student_voter->firstname . " " . $student_voter->lastname }}</h4><br><br>
{!! Form::open(['route' => ['votes.store'], 'Method' => 'POST']) !!}

@foreach($candidates_array as $positions)
	@if($positions[0]->position->limitation == "none" || $positions[0]->position->limitation == $student_voter->course)
	<div class = "vote_container">
		<h1> {{ $positions[0]->position->description}} </h1>
		@if ($positions[0]->position->is_multiple == "1")
			<h3> Max Vote of {{$positions[0]->position->multiple_limit}}</h3>
		@endif
		<table class = "table">
			<tbody>
			@foreach($positions as $candidate)
					<tr>
						<div class = "row">
							<img class="img-circle" src="{{url($candidate->img_url)}}" width = "200" height = "200" onerror="this.src='{{url("/images/abstain_img.jpeg")}}'">
							<h3>{{ $candidate->lastname . ", " .$candidate->firstname }}</h3>
							@if ($positions[0]->position->is_multiple == "1")
								<input class = "row_vote" type = "checkbox" name = "checkbox-{{$positions[0]->position->position_level}}[]" value="{{ $candidate->id }}">
							@else
								<input class = "row_vote" type = "radio" name = "radiobutton['{{$positions[0]->position->position_level}}']" value="{{ $candidate->id }}" checked="true">
							@endif
						</div>
					</tr>
					<br>
			@endforeach
			</tbody>
		</table>
	</div>
	@else
		@continue
	@endif
@endforeach
<br>
{!! Form::hidden('student_id', $student_voter->id) !!}
{!! Form::hidden('event_id', $event_id) !!}
{!! Form::submit('Submit', ['class' => 'btn btn-primary btn-large btn-block', 'id' => 'submit']) !!}
{!! Form::close() !!}
<button id = "prev" class = "btn btn-primary">Previous</button>
<button id = "next" class = "btn btn-primary">Next</button>
<br><br><br><br>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		$("#submit").hide();
		$(".vote_container").hide();
		
		// Show the first div with class vote_container
		var current_div;
		if(current_div == null){
			var current_div = $(".vote_container:first").fadeIn(700);
		}

		// Go to next candidates and apply transition
		$("#next").click(function(){
			if(current_div.next(".vote_container").length > 0){
				current_div.slideUp(700, function(){
				current_div.hide();
				current_div = current_div.next().slideDown(700);
				});
			}
			else{
				$("#next").hide();
				$("#prev").hide();
				$("#submit").show();
			}
		});

		// Go back to previous candidates and apply transition
		$("#prev").click(function(){
			if(current_div.prev(".vote_container").length > 0){
				current_div.slideUp(700, function(){
				current_div.hide();
				current_div = current_div.prev().slideDown(700);
				});
			}
			else{
				
			}
		});

		// Selects the radio button when .row div is selected by the mouse
		$('.row').click(function (event) {
			if (event.target.type !== 'radio') {
				$(':radio', this).trigger('click');
			}
		});
		
		$("input[type='radio']").change(function (e) {
			e.stopPropagation();
			$('.row').removeClass("highlight-row");
			if ($(this).is(":checked")) {
				$(this).closest("div").addClass("highlight-row");
			}
    	});
	});
</script>
@endsection

