
@extends('layouts.layouts_v')

@section('title', 'Please vote wisely!')

@section('cover', 'Results')

@section('content')
@foreach($candidates_array as $positions)
	<h4 id = "position_header">Results for position {{ $positions[0]->position->description}} </h4>
	<table class = "table table-condensed table-bordered">
		<tbody>
		@foreach($positions as $candidate)
			<tr>
				<td><h5>{{ $candidate->lastname . ", " .$candidate->firstname }}</h5></td>
				<td><h5>{{ $votescount_array[$candidate->id] }}</h5></td>
			</tr>
		@endforeach
		</tbody>
	</table>
@endforeach
<button class = "btn btn-success btn-block"><a href = "{{ URL::previous() }}">Return to Event</a></button> 
<br><br><br><br>
@endsection

<style>
body{
	font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif;
}
.row{
	border-radius: 5px;
	background-color:#606060;
	padding: 10px 0 10px 0;
}
.img-circle{
	border: 5px solid #333;
}
#position_header{
	margin: auto;
	background-color: #fff;
	color: #000;
	padding: 5px;
}
.table{
	background-color: #fff;
	color: #000;
}
</style>