@extends('layouts.layouts')

@section('content')
<div>
	{!! Form::open(['route' => 'positions.store', 'Method' => 'POST', 'class' => 'form-inline']) !!}
    {!! Form::label('position', 'Position Description:') !!}
    {!! Form::date('description', null, ['id' => 'description', 'class' => 'form-control', 'required' => '', 'maxlength' => '50']) !!}
    {!! Form::label('position_level', 'Position Level:') !!}
    {!! Form::date('level', null, ['id' => 'level', 'class' => 'form-control', 'required' => '', 'maxlength' => '50']) !!}
    {!! Form::!!}
    {!! Form::submit() !!}
</div>
@endsection