@extends('layouts.layouts')

@section('title', '| Events')

@section('nav_title', 'Event')

@section('content')

@if(Session::has('message'))
<div class="alert alert-success">
    {{ Session::get('message') }}
</div>
@endif

@if(count( $errors ) > 0)
    <ul class="alert alert-danger">
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif

<h1 class="page-header">Events</h1>
<div class="row pull-left">
    @can('Add')
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addEvent">
        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New Event
    </button>
    @endcan
</div>

<div id = "event_table">
    <table class = 'table table-hover'>
        <thead>
            <th> Description </th>
            <th> Start  </th>
            <th> End </th>
            <th> Status </th>
            <th> Action </th>
        </thead>
        <tbody>
        @foreach($events as $event)
            <tr>
                <td> {{ $event -> description}} </td>
                <td> {{ $event -> start_date}} </td>
    	        <td> {{ $event -> end_date }} </td>
                <td>  {{ $event -> status }}</td>
                <td>
                    <a href="{{ route('events.show', $event->id) }}" type="button" class="btn btn-success" aria-label="Left Align">
                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                    </a>
                    @can('Edit')
                    <button class="edit-modal btn btn-success" data-toggle="modal" data-target="#editEvent" data-id="{{ $event->id }}" data-description="{{ $event->description }}" data-startdate="{{ $event->start_date }}" data-enddate="{{ $event->end_date }}">
                    <span class="glyphicon glyphicon-pencil"></span>
                    </button>
                    @endcan
                    <a href="{{ route('votes.show', $event->id) }}" type="button" class="btn btn-success" aria-label="Left Align">
                    <span class="glyphicon glyphicon-share" aria-hidden="true"></span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $events->links() }}
</div>
@endsection

@section('modal')
<!-- Edit Event Modal -->
<div id="editEvent" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="panel panel-primary">
              <div class="panel-heading">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="panel-title" id="myModalLabel"><b>Edit Event</b></h4>
              </div>
            <div class="modal-body">
              @if(isset($event->id))
                {!! Form::model($event, ['route' => ['events.update', $event->id], 'method' => 'PUT']) !!}
                    {!! Form::hidden('id', null, ['id' => 'uid', 'class' => 'form-control', 'required' => '']) !!}

                    {!! Form::label('description', 'Description:') !!}
                    {!! Form::text('description', null, ['id' => 'eventdesc', 'class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

                    {!! Form::label('start_date', 'Start Date:') !!}
                    {!! Form::date('start_date', null, ['id' => 'eventstart', 'class' => 'form-control', 'required' => '']) !!}

                    {!! Form::label('end_date', 'End Date:') !!}
                    {!! Form::date('end_date', null, ['id' => 'eventend', 'class' => 'form-control', 'required' => '']) !!}

                    <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal" style="margin-top:20px">Cancel</button>
                      {!! Form::submit('Update', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}
                    </div>
                {!! Form::close() !!}
              @endif
            </div>
          </div>
        </div>
    </div>

<!-- Add Event Modal -->
    <div class="modal fade" id="addEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="panel panel-primary">
          <div class="panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="panel-title" id="myModalLabel"><b>Add Event</b></h4>
          </div>
          <div class="modal-body">
            {!! Form::open(['route' => 'events.store', 'Method' => 'POST']) !!}

            {!! Form::label('Event Description') !!}
            {!! Form::text('description', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

            {!! Form::label('Start Date') !!}
            {!! Form::date('start_date', null, ['class' => 'form-control', 'required' => '']) !!}

            {!! Form::label('End Date') !!}
            {!! Form::date('end_date', null, ['class' => 'form-control', 'required' => '']) !!}

            {!! Form::submit('Save', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}

            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
@endsection

@section('scripts')
@parent
{{-- Edit Event Modal --}}
<script type="text/javascript">
    $(document).on('click', '.edit-modal', function() {
        $('#uid').val($(this).data('id'));
        $('#eventdesc').val($(this).data('description'));
        $('#eventstart').val($(this).data('startdate'));
        $('#eventend').val($(this).data('enddate'));
        $('#editEvent').modal('show');
    });
</script>
@endsection
