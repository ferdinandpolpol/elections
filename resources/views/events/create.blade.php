@extends('layouts.layouts')

@section('title', 'Create Event')

@section('content')
	<div class="container">
	<h3> Event </h3>
	{!! Form::open(['route' => 'events.store', 'Method' => 'POST']) !!}

		{!! Form::label('Event Description') !!}
		{!! Form::text('description', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

		{!! Form::label('Start Date') !!}
		{!! Form::date('start_date', null, ['class' => 'form-control', 'required' => '']) !!}
		{!! Form::label('End Date') !!}
		{!! Form::date('end_date', null, ['class' => 'form-control', 'required' => '']) !!}

		{!! Form::submit('Save', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}

	{!! Form::close() !!}
	</div>
	@if(count( $errors ) > 0)
		<ul class="alert alert-danger">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	@endif
@endsection
	
@section('scripts')
	<script type = "text/javascript">
	</script>
@endsection