 @extends('layouts.layouts')

@section('title', 'Event Details')

@section('nav_title', 'Event Details')

@section('sidebar')
<li>
	<a href = "{{ route('votes.show', $events->id) }}"> Initiate Voting! </a>
</li>
@endsection

@section('content')
	<div class="container">
		<h3 style="margin-bottom: 30px; text-transform: uppercase; font-size: 50px; font-weight: 10px; text-align: center"> {{ $events -> description }} 
		</h3>

		@if(Session::has('message'))
			<div class="alert alert-success">
				{{ Session::get('message') }}
			</div>
		@endif
		@if(count( $errors ) > 0)
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
		@endif	

		<div class="row" >
			<div class="col-md-4">
			@can('Add')
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#addPosition">
		        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Position
		    	</button>
			@endcan
			@can('View')
		    	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#showPosition">
		        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Available Position
		    	</button>
			@endcan
			</div>
	</div>
    	
		

		<h3> List of Candidates </h3>
		<button class="btn btn-success" id="toggleCandidates">
		<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
		</button>
		
		<br><br>
		<div id="candidatesList">
		<table class = 'table table-hover' id="candidatesList">
			<thead>
				<tr>
					<th>Lastname</th>
					<th>Firstname</th>
					<th>Position</th>
					<th>Action</th>
				</tr>
			</thead>

			<tbody>
				@foreach($candidates as $candidate)
					<tr>
						<td> {{ $candidate->lastname }}</td>
						<td> {{ $candidate->firstname }}</td>
						<td> {{ $candidate->position->description }}</td>
						<td> 
							
							@can('Edit')
							{!! Form::open(['route' => ['candidates.destroy', $candidate->id], 'Method' => 'POST', 'onsubmit' => 'return ConfirmDelete()']) !!}
		                    {{ method_field('DELETE') }}
							<button type = "button" class="edit-candidate-modal btn btn-success" data-toggle="modal" data-target="#editCandidate" data-id="{{ $candidate->id }}" data-firstname="{{ $candidate->firstname }}" data-lastname="{{ $candidate->lastname }}" data-positiondesc="{{ $candidate->position->id }}" data-image="{{ $candidate->img_url }}">
							<span class="glyphicon glyphicon-pencil"></span>
							</button>

							<button type="submit" class="btn btn-danger" aria-label="Left Align">
							<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							</button>
							{!! Form::close() !!}
							@endcan
						
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		{{ $candidates->links() }}
		</div>
		
		<br>
		@can('Add')
		<button type="button" class="btn btn-success" data-toggle="modal" data-target="#addCandidate" style="margin-bottom: 30px;">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New Candidate
		</button>
		@endcan

		@can('Add')
		<div class="row" >
			<div class="col-md-4" style="border-radius: 5px; border-width: 5px; border-color: black; background-color: #f7f9f7; box-shadow: 0 1px 2px;padding: 10px; width: 210px; margin-bottom: 30px">
				{!! Form::open(['route' => 'students.store', 'Method' => 'POST', 'files' => true]) !!}
				{!! Form::label('Student List') !!} 
				{!! Form::file('thefile') !!}
				{!! Form::hidden('event_id', $events->id) !!}
				{!! Form::submit('Submit', ['class' => 'btn btn-default btn-block', 'style' => 'margin-top: 20px']) !!}
				{!! Form::close() !!}
			</div>
			<div class="col-md-6">
				<blockquote>
					"Upload students list with an Excel file.
					The file's first row must contain the tables; code, firstname, lastname, course, and year
					for the system to read it"
				</blockquote>
			</div>
		</div>
		@endcan

		@can('View')
	  	<h3> Students List </h3>
	  		<div class="row">
				<div class="col-md-8">
					<button class="btn btn-success" id="toggleStudents">
					<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
					</button>
				</div>
				<div class="col-md-4">
					<form action="{{ route('students.index') }}" method="get" role="search">
						<div class="input-group ">
							<input type="text" class="form-control" name="search" placeholder="Keyword">
							<input type="hidden" class="form-control" name="id" value="{{ $events->id }}">
							<button type="submit" class="btn btn-default">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						</div>
					</form>
				</div>
			<br><br>

	  	{!! Form::open(['route' => ['students.destroy', $events->id], 'Method' => 'POST', 'onsubmit' => 'return ConfirmDelete()']) !!}
			{{ method_field('DELETE') }}
			<button type="submit" class="btn btn-danger">
				<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Clear Student List
			</button>
			{!! Form::close() !!}	

		<br>
		<div id = 'studentList'>
	  	<table class = 'table table-hover' >
			<thead>
				<tr>
					<th>Code</th>
					<th>Name</th>
					<th>Course</th>
					<th>Has Voted</th>
				</tr>
			</thead>

			<tbody>
				@foreach($students as $student)
					<tr>
						<td> {{ $student->student_code }}</td>
						<td> {{ $student->lastname . ", " . $student->firstname }}</td>
						<td> {{ $student->course }}</td>
						@if($student->has_voted == 0)
							<td> Not Yet </td>
						@else
							<td class='success'> Voted </td>
						@endif
					</tr>
				@endforeach
			</tbody>
		</table>

		{{ $students->links() }}
		</div>
		
		@endcan
		@if($events->status == "Active")
		<a href="{{ route('votes.show', $events->id) }}"><button type="button" class="btn btn-primary large btn-block">Initiate Voting</button></a>
		@endif
		<br>
		@can('View Results')
		<a href="{{ route('results.show', $events->id) }}"><button type="button" class="btn btn-primary large btn-block">Result</button></a>
		@endcan
	</div>
	<br><br><br>
@endsection
@section('modal')
<!-- Add Candidate Modal -->
    <div class="modal fade" id="addCandidate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="panel panel-primary">
          <div class="panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="panel-title" id="myModalLabel"><b>Add Candidate</b></h4>
          </div>
          <div class="modal-body">
            {!! Form::open(['route' => 'candidates.store', 'Method' => 'POST', 'files' => true]) !!}

			{!! Form::label('Lastname') !!}
			{!! Form::text('lastname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

			{!! Form::label('Firstname') !!}
			{!! Form::text('firstname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}
			
			{!! Form::label('Position') !!}
			<br>
			{!!  Form::select('position_id', $positions1, null,['class' => 'form-control']) !!}
			<br><br>
			{!! Form::label('Select image to upload:') !!}
			{!! Form::file('image', ['required' => '']) !!}

			{!! Form::hidden('event_id', $events->id) !!}
			
			{!! Form::submit('Save', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}

			{!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
<!-- Add Position Modal -->
    <div class="modal fade" id="addPosition" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="panel panel-primary">
          <div class="panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="panel-title" id="myModalLabel"><b>Add Position</b></h4>
          </div>
          <div class="modal-body">
          	{!! Form::open(['route' => 'positions.store', 'Method' => 'POST']) !!}

			{!! Form::label('Position Title:') !!}
			{!! Form::text('description', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

			{!! Form::label('Position Level:') !!}
			{!! Form::number('position_level', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '2', 'min' => '1', 'max' => '20']) !!}


			{!! Form::label('Multiple Votes:') !!}
			{!! Form::select('is_multiple', array('1' => 'Yes', '0' => 'No'), '0', ['id'=> 'is_multiple', 'class' => 'form-control', 'required' => '']) !!}

			{!! Form::label('multiple_limit','Multiple Votes Limitation:', ['class' => 'multiple-limit']) !!}
			{!! Form::number('multiple_limit', null, ['class' => 'form-control multiple-limit', 'maxlength' => '2', 'min' => '1', 'max' => '99']) !!}

			{!! Form::label('Division Limitation:') !!}
			{!! Form::select('limitation', array_merge(array("none" => "None"), $courses), "None",['class' => 'form-control', 'required' => '']) !!}

			{!! Form::hidden('event_id', $events->id) !!}
			
			{!! Form::submit('Save', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}

			{!! Form::close() !!}
			@if(count( $errors ) > 0)
                <ul class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif	
          </div>
        </div>
      </div>
    </div>
<!-- Show Position Modal -->
<div class="modal fade" id="showPosition" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="panel panel-primary">
          <div class="panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="panel-title" id="myModalLabel"><b>Available Positions</b></h4>
          </div>
          <div class="modal-body">
          	<table class = 'table table-hover'>
			<thead>
				<tr>
					<th>Position Title</th>
					<th>Position Level</th>
					<th>Action</th>
				</tr>
			</thead>

			<tbody>
				@foreach($positions as $position)
					<tr>
						<td> {{ $position->description }}</td>
						<td> {{ $position->position_level }}</td>
						<td> 
							<button class="edit-position-modal btn btn-success" data-toggle="modal" data-target="#editPosition" data-id="{{ $position->id }}" data-description="{{ $position->description }}" data-level="{{ $position->position_level }}" data-event-id="{{ $position->event_id }}">
		                    	<span class="glyphicon glyphicon-pencil"></span>
		                    </button>

							{!! Form::open(['route' => ['positions.destroy', $position->id], 'Method' => 'POST', 'onsubmit' => 'return ConfirmDelete()']) !!}
		                    {{ method_field('DELETE') }}
								<button type="submit" class="btn btn-danger" aria-label="Left Align">
	  								<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</button>
							{!! Form::close() !!}
						
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
          </div>
        </div>
      </div>
    </div>
<!-- Edit Position Modal -->
<div id="editPosition" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="panel panel-primary">
              <div class="panel-heading">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="panel-title" id="myModalLabel"><b>Edit Position</b></h4>
              </div>
            <div class="modal-body">
              @if(isset($position->id))
                {!! Form::model($position, ['route' => ['positions.update', $position->id], 'method' => 'PUT']) !!}
                    {!! Form::hidden('id', null, ['id' => 'uid', 'class' => 'form-control', 'required' => '']) !!}

                    {!! Form::label('description', 'Position Title:') !!}
                    {!! Form::text('description', null, ['id' => 'postitiondesc', 'class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

                    {!! Form::label('position_level', 'Position Level:') !!}
                    {!! Form::number('position_level', null, ['id' => 'positionlevel', 'class' => 'form-control', 'required' => '', 'maxlength' => '2', 'min' => '1', 'max' => '20']) !!}

                    {!! Form::hidden('event_id', $events->id) !!}

                    <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal" style="margin-top:20px">Cancel</button>
                      {!! Form::submit('Update', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}
                    </div>
                {!! Form::close() !!}
              @endif
            </div>
          </div>
        </div>
    </div>
<!-- Edit Candidate Modal -->
<div id="editCandidate" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="panel panel-primary">
              <div class="panel-heading">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="panel-title" id="myModalLabel"><b>Edit Candidate</b></h4>
              </div>
            <div class="modal-body">
              @if(isset($candidate->id))
                {!! Form::model($candidate, ['route' => ['candidates.update', $candidate->id], 'method' => 'PUT', 'files' => true]) !!}

                {!! Form::hidden('id', null, ['id' => 'candid', 'class' => 'form-control', 'required' => '']) !!}
                   
                {!! Form::label('Lastname') !!}
								{!! Form::text('lastname', null, ['id' => 'lastname', 'class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

								{!! Form::label('Firstname') !!}
								{!! Form::text('firstname', null, ['id' => 'firstname', 'class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}
								
								{!! Form::label('Position') !!}
								<br>
								{!!  Form::select('position_id', $positions1, null, ['id' => 'position', 'class' => 'form-control']) !!}
								<br><br>
								<p> This is the current image used </p>
								<img class = "img img-circle" width="200" height="200" id = "currimg"><br>
								{!! Form::label('Select image to upload:') !!}
								{!! Form::file('image') !!}
								{!! Form::hidden('event_id', $events->id) !!}
								<div class="modal-footer">
								<button type="button" class="btn btn-primary" data-dismiss="modal" style="margin-top:20px">Cancel</button>
								{!! Form::submit('Update', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}
								</div>
								{!! Form::close() !!}
              @endif
            </div>
          </div>
        </div>
    </div>
@endsection
	
@section('scripts')
@parent
<!--  Edit Position Modal -->
<script type="text/javascript">
    $(document).on('click', '.edit-position-modal', function() {
        $('#uid').val($(this).data('id'));
        $('#positiondesc').val($(this).data('description'));
        $('#positionlevel').val($(this).data('position_level'));
        $('#editPosition').modal('show');
    });
    $(document).on('click', '.edit-candidate-modal', function() {
        $('#candid').val($(this).data('id'));
        $('#firstname').val($(this).data('firstname'));
        $('#lastname').val($(this).data('lastname'));
        $('#position').val($(this).data('positiondesc'));
        $('#currimg').attr("src", $(this).data('image'));
        $('#editCandidate').modal('show');
    });
    function ConfirmDelete()
		{
			var x = confirm("Are you sure you want to delete?");
			if (x)
				return true;
				else
					return false;
		}

		$(function(){
			$('#toggleCandidates').click(function(){
				$('#candidatesList').slideToggle(500);
			});

			$('#toggleStudents').click(function(){
				$('#studentList').slideToggle(500);
			});
		});
</script>

<script>
	$(".multiple-limit").hide();
	$('#is_multiple').change(function(){
		$(".multiple-limit").toggle();
	});
</script>		
@endsection