@extends('layouts.layouts')

@section('title', 'Users')

@section('nav_title', 'Users')

@section('content')
<div class="container">
		<h3 style="margin-bottom: 30px; text-transform: uppercase; font-size: 50px; font-weight: 10px; text-align: center">
		</h3>

		@if(Session::has('message'))
			<div class="alert alert-success">
				{{ Session::get('message') }}
			</div>
		@endif
		@if(count( $errors ) > 0)
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif	

		<div class="row" >
		
		<h3> Users </h3>

		@can('Add')
		<button type="button" class="btn btn-success" data-toggle="modal" data-target="#addUser" style="margin-bottom: 30px;">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add User
		</button>
		@endcan

		<table class = 'table table-hover'>
			<thead>
				<tr>
					<th>Name</th>
					<th>E-Mail</th>
					<th>Role</th>
					<th>Action</th>
				</tr>
			</thead>

			<tbody>
				@foreach($users as $user)
					<tr>
						<td> {{ $user->username }}</td>
						<td> {{ $user->email }}</td>
						<td> {{ $user->rolename }} </td>
						<td> 
							
							@can('Edit')
		                    {!! Form::open(['route' => ['users.destroy', $user->id], 'Method' => 'POST', 'onsubmit' => 'return ConfirmDelete()']) !!}
		                    {{ method_field('DELETE') }}
								<button type="button" class="edit-user-modal btn btn-success" data-toggle="modal" data-target="#editUser" data-id="{{ $user->id }}" data-username="{{ $user->username }}" data-password="{{ $user->password }}" data-email="{{ $user->email }}" data-rolename="{{ $user->rolename }}">	
								<span class="glyphicon glyphicon-pencil"></span>
								</button>

			                    <button type="submit" class="btn btn-danger">
			                    <span class="glyphicon glyphicon-trash"></span>
			                    </button>
			                {!! Form::close() !!}	
							@endcan				
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
@endsection

@section('modal')
<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="panel-title" id="myModalLabel"><b>Add Event</b></h4>
		</div>
		<div class="modal-body">
		{!! Form::open(['route' => 'users.store', 'Method' => 'POST']) !!}

		{!! Form::label('User Name') !!}
		{!! Form::text('name', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

		{!! Form::label('E-Mail') !!}
		{!! Form::email('email', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

		{!! Form::label('Password') !!}
		{!! Form::password('password',['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

		{!! Form::label('Role') !!}<br>
		{!!  Form::select('role_id', $roles, null, ['class' => 'form-control', 'required' => '']) !!}
		<br>
		<br>
		{!! Form::submit('Save', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}

		{!! Form::close() !!}
		</div>
	</div>
	</div>
</div>

<!-- Edit User Modal -->
<div id="editUser" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="panel panel-primary">
              <div class="panel-heading">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="panel-title" id="myModalLabel"><b>Edit User</b></h4>
              </div>
            <div class="modal-body">
              @if(isset($user->id))
                {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'PUT']) !!}
                    {!! Form::hidden('id', null, ['id' => 'uid', 'class' => 'form-control', 'required' => '']) !!}

                    {!! Form::label('name', 'User Name:') !!}
                    {!! Form::text('name', null, ['id' => 'username', 'class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

                    {!! Form::label('email', 'E-Mail:') !!}
                    {!! Form::email('email', null, ['id' => 'email', 'class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

                  	{!! Form::label('Role') !!}<br>
					{!!  Form::select('role_id', $roles, null, ['id' => 'role', 'class' => 'form-control', 'required' => '']) !!}

                    <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal" style="margin-top:20px">Cancel</button>
                      {!! Form::submit('Update', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}
                    </div>
                {!! Form::close() !!}
              @endif
            </div>
          </div>
        </div>
    </div>
@endsection

@section('scripts')
@parent
<!--  Edit Position Modal -->
<script type="text/javascript">
    $(document).on('click', '.edit-user-modal', function() {
        $('#uid').val($(this).data('id'));
        $('#username').val($(this).data('username'));
        $('#email').val($(this).data('email'));
        $('#role').val($(this).data('role_id'));
        $('#editPosition').modal('show');
    });
    function ConfirmDelete()
		{
			var x = confirm("Are you sure you want to delete?");
			if (x)
				return true;
				else
					return false;
		}
</script>
@endsection