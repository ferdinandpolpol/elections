@extends('layouts.layouts')

@section('title', 'Update Candidate')

@section('content')
	<div class="container">
	<h3> Update Candidate </h3>
	{!! Form::model($candidates, ['route' => ['candidates.update', $candidates->id]]) !!}

		{!! Form::label('Lastname') !!}
		{!! Form::text('lastname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

		{!! Form::label('Firstname') !!}
		{!! Form::text('firstname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

		{!! Form::label('Alias') !!}
		{!! Form::text('alias', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

		{!! Form::label('Position') !!}
		{!! Form::text('position', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

		{!! Form::label('Platform') !!}
		{!! Form::text('platform', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

  		{{ method_field('PUT') }}﻿
  		
		{!! Form::submit('UPDATE', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}

	{!! Form::close() !!}
	</div>
	@if(count( $errors ) > 0)
		<ul class="alert alert-danger">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	@endif
@endsection

@section('scripts')
	<script type = "text/javascript">
	</script>
@endsection