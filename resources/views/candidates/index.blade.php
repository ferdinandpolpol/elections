@extends('layouts.layouts')

@section('title', 'Student List')

@section('content')

	<div class = "container">
		<h3> List of Candidates </h3>
		@if(Session::has('message'))
			<div class="alert alert-success">
				{{ Session::get('message') }}
			</div>
		@endif

		<a class="btn btn-primary" href="{{ route('candidates.create') }}" role="button">Add Candidate</a>

		<table class = 'table table-hover'>
			<thead>
				<tr>
					<th>Lastname</th>
					<th>Firstname</th>
					<th>Position</th>
					<th>Action</th>
				</tr>
			</thead>

			<tbody>
				@foreach($candidate as $candidate)
					<tr>
						<td> {{ $candidate->lastname }}</td>
						<td> {{ $candidate->firstname }}</td>
						<td> {{ $candidate->position }}</td>
						<td> 
							<a href="{{ 'candidates/'. $candidate->id .'/edit' }}" type="button" class="btn btn-success" aria-label="Left Align">
  								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
							</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		{{-- {{ $candidate->links() }} --}}
	</div>

@endsection