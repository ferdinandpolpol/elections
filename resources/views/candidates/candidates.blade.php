@extends('layouts.layouts')

@section('title', 'Create Candidate')

@section('content')
	<div class="container">
	<h3> Candidate Details </h3>
	{!! Form::open(['route' => 'candidates.store', 'Method' => 'POST']) !!}

		{!! Form::label('Lastname') !!}
		{!! Form::text('lastname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

		{!! Form::label('Firstname') !!}
		{!! Form::text('firstname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

		{!! Form::label('Position') !!}
		{!!  Form::select('select', ['1' => 'President', '2' => 'Vice President', '3' => 'Secretary', '4' => 'treasurer'],  'S', ['class' => 'form-control' ]) !!}

		{!! Form::submit('Save', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}

	{!! Form::close() !!}
	</div>
	@if(count( $errors ) > 0)
		<ul class="alert alert-danger">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	@endif
@endsection

@section('scripts')
	<script type = "text/javascript">
	</script>
@endsection