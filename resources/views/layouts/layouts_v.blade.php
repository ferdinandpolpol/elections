<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>Voting | @yield('title') </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="{{ asset('css/cover.css') }}" rel="stylesheet">

</head>

<body>
  <div class="site-wrapper">
    <div class="site-wrapper-inner">
      <div class="cover-container">
        <div class="masthead clearfix">
          <div class="inner">
            <h3 class="masthead-brand">@yield('brand')</h3>
          </div>
        </div>

        <div class="inner cover">
          <h1 class="cover-heading">@yield('cover')</h1>
          @yield('content')
        </div>

        <div class="mastfoot">
            <p>Lead Developer: Ferdinand A. Polpol | Co-Developer: John Carlos L. Ochave</p>
        </div>
      </div>
    </div>
  </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"><script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
@yield('script')
</body></html>

<style>
html, body{
  background-color: #21255e;
  font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif;
	background-image: url('/images/bg/spc.jpg');
	background-repeat: no-repeat;
	background-size: 100% 100%;
}
</style>