<!DOCTYPE html>
<html lang="en">
<head>

    <link rel="icon" href="https://getbootstrap.com/docs/3.3/favicon.ico">

	<title>
		San Pedro College Commission on Elections | @yield('title')
	</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">

</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="https://getbootstrap.com/docs/3.3/examples/dashboard/#">
          	San Pedro College Commission on Elections | @yield('nav_title')
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="{{ route('logout') }}"
            onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
            Logout
              </a>
            </li>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <img src = "{{ url('/images/logos/spc.png') }}" class="image-responsive nav-img">
          <img src = "{{ url('/images/logos/comelec.png') }}" class="image-responsive nav-img">
          <ul class="nav nav-sidebar">
            @if(Auth::user())
              <h4><center> Welcome, </center></h4>
             <center><h3>{{ Auth::user()->name }}</h3></center>
            @endif
            <li><a href = "{{ route('events.index') }}"> Events </a></li>
            @can('View')
              <li><a href = "{{ route('users.index') }}"> Users </a></li>
            @endcan
          	@yield('sidebar')
            <li>
              <a target = "_blank" href = "https://docs.google.com/document/d/1_M9ZRH0QB4XsujIhUEZtb08pFNVya_AzNyCQl8Dx_1c/edit?usp=sharing"><h6>Developer Notes</h6></a>
            </li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        	@yield('content')
        </div>
      </div>
    </div>

    <nav class="navbar navbar-inverse navbar-fixed-bottom">
      <div class="container-fluid">
        <div class="navbar-header">
             <h6>Lead Developer: Ferdinand A. Polpol | Co-Developer: John Carlos L. Ochave</h6>
        </div>
      </div>
    </nav>

@yield('modal')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
@yield('scripts') 
</body>
</html>

<style>
html, body{
  font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif;
}
.navbar{
  background-color:	#21255e;
  color: #fff;
}
.navbar-fixed-bottom{
   background-color:	#21255e;
}

.nav-img{
  width: 100px;
  height: 130px;
  margin-left: 25px;
  margin-right: auto;
}
</style>