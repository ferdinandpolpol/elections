<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStudentHasvotedColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Drop the column has_voted if already existing, usable if rolling back
        if(Schema::hasColumn('students', 'has_voted')){
            Schema::table('students', function($table){
                $table->dropColumn('has_voted');
            });
        }
        Schema::table('students', function($table){
            $table->boolean('has_voted')->default(0);
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
