<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPositionLevelColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('positions', 'position_level')){
            Schema::table('positions', function($table){
                $table->dropColumn('position_level');
            });
        }
        Schema::table('positions', function($table){
            $table->integer('position_level')->after('description');
            $table->integer('event_id')->after('position_level')->unsigned();

            $table->foreign('event_id')->references('id')->on('events')->onCascade('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
