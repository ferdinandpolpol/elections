<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Insert into Users Table
		DB::table('users')->insert([
            'name' => 'fapolpol',
            'email' => 'ferdinand.polpol@gmail.com',
            'password' => bcrypt('123456'),
            'status' => 0,
        ]);

		//Insert into Roles Table
        DB::table('roles')->insert([
            'name' => 'Administrator',
            'description' => 'All Rights',
            'created_at'    => \Carbon\Carbon::now(),
            'updated_at'    => \Carbon\Carbon::now()
        ]);
        DB::table('roles')->insert([
            'name' => 'Moderator',
            'description' => 'Manage the system',
            'created_at'    => \Carbon\Carbon::now(),
            'updated_at'    => \Carbon\Carbon::now()
        ]);

        //Insert into Permissions Table, all permissions are added here
        DB::table('permissions')->insert([
            'name' 			=> 'Add',
            'description' 	=> 'Add Users,Event, Position, Candidate, Students',
            'created_at' 	=> \Carbon\Carbon::now(),
            'updated_at' 	=> \Carbon\Carbon::now()
        ]);
        DB::table('permissions')->insert([
            'name' 			=> 'Edit',
            'description' 	=> 'Edit Event, Positions, Candidates',
            'created_at' 	=> \Carbon\Carbon::now(),
            'updated_at' 	=> \Carbon\Carbon::now()
        ]);
        DB::table('permissions')->insert([
            'name' 			=> 'View',
            'description' 	=> 'View Users, events',
            'created_at' 	=> \Carbon\Carbon::now(),
            'updated_at' 	=> \Carbon\Carbon::now()
        ]);
        DB::table('permissions')->insert([
            'name' 			=> 'View Results',
            'description' 	=> 'View Results',
            'created_at' 	=> \Carbon\Carbon::now(),
            'updated_at' 	=> \Carbon\Carbon::now()
        ]);

        
            
        //Assign user Super Administrator the role Administrator
        $user = User::find(1);
        $user->roles()->attach(1);

        //Assign role Administrator all the permissions.
        $role = Role::find(1);
        $role->permissions()->attach([1,2,3,4]);

        
        $role = Role::find(2);
        $role->permissions()->attach(3);
    }
}
