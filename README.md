# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for creating a basic elections system that is based on events.

### How do I get set up? ###

The setup will need an Excel file .xlsx or .csv file to be able to use properly.

The system will depend heavily on events and are differentiated per event.

1. Register an account

2. Create an Election Event, the dates must be starting from Today and will end on the Users choice

3. Create Positions and their corresponding levels. (e.g. President = 1, Vice President = 2, so on)

4. Create Candidates on the event. The positions will correspond to the Positions created

5. Upload your provided Spreadsheet file. The file's first row must contain the following: code, firstname, lastname, course, year

6. Users can now Initiate the voting sequence.

7. On the VOTING page, the students from the list can enter based on their 'code' values

8. After the voting is done, we can now view the RESULTS on the bottom of the page.

### Future Updates ###

1. Editable images for the candidates

2. Fix various bugs and validations

3. Fix the various unclickable buttons

4. Make Roles and Permissions for the Users. Set that only the admin can add and edit, and there will be Moderators to handle the Initiate Voting

### Who do I talk to? ###

Developers: Ferdinand A. Polpol, John Carlos L. Ochave


### Developer Installation ###
# download composer
# run composer-install
# change apache/conf/extra/httpd-vhosts and add a route that directs to elections/public
# change hosts file in syste32 with Administrator Rights
# comment out PermissionsServiceProvider
# create 'elections' in phpmyadmin
# php artisan migrate:install
# uncomment PermissionsServiceProvider
# <VirtualHost *:80>
    DocumentRoot "C:/xampp/htdocs/elections/public"
    ServerName elections.dev
    ServerAlias elections.dev
    ErrorLog "logs/elections.log"
    CustomLog "logs/custom.elections.log" combined
    <Directory "C:/xampp/htdocs/elections/public">
        AllowOverride All
        Order Allow,Deny
        Allow from all
        Require all granted
    </Directory>
</VirtualHost>
# php artisan key:generate
# php artisan migrate

