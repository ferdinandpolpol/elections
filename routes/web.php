<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if(Auth::user()){
    	return redirect()->route('events.index');
	}
	else{
		return redirect()->route('login');
	}
});
/*Route::get('/', function(\Illuminate\http\Request $request){
	$user = $request->user();
	//dd($user->hasRole('Admin'));
	//Change to when having can
	dd($user->can('Add Candidate'));
});*/

// Authenticated Web Pages
//Route::group(['middleware' => 'auth'], function(){
Route::resource('candidates', 'CandidateController');
Route::resource('students', 'StudentController');
Route::resource('positions', 'PositionController');
Route::resource('results', 'ResultController');
Route::resource('votes', 'VoteController');
Route::resource('events', 'EventController');
Route::resource('users', 'UserController');
Route::post('votes/{votes}/validate', 'VoteController@ValidateStudent')->name('votes.ValidateStudent');
Route::post('votes/{votes}/start', 'VoteController@StartVoting')->name('votes.StartVoting');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');