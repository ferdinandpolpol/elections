<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{


    public $timestamps = false;
    
    protected $fillable = ['lastname', 'firstname', 'position_id', 'event_id', 'image'];

    public function position(){
        return $this->belongsTo('App\Position');
    }
}
