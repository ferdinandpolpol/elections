<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $fillable = ['description', 'position_level'];
    public function candidates(){
    	return $this->hasMany(Candidate::class, 'candidates');
    }
}
