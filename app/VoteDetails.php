<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoteDetails extends Model
{
    protected $table = 'votedetails';

    public function vote(){
        return $this->belongsTo('App\Vote');
    }
}
