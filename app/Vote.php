<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    public $timestamps = false;

    public function vote_details(){
        return $this->hasMany('App\VoteDetails');
    }
}
