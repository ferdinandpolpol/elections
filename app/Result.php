<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    //
    protected $fillable = ['candidate_id', 'img_url', 'total_votes', 'event_id'];
}
