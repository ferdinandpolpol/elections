<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

    public $timestamps = false;

    public function event(){
    	return $this->belongsTo('App\Event', 'event_id');
    }

    public function search($query, $s){
    	return $query->where('name', 'like', '%'. $s .'%');
    }
}
