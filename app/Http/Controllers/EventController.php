<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Candidate;
use App\Student;
use App\Position;
use Carbon\Carbon; 
use Auth;
use Session;

class EventController extends Controller
{
    public function index()
    {
        if(Auth::user()){
            $events = Event::simplePaginate(100);
            $today_date = Carbon::now();
            
            foreach ($events as $event) {
                $enddate = Carbon::createFromFormat('Y-m-d', $event-> end_date);
                $data_difference = $today_date->diffInDays($enddate, false);

                if($data_difference > 0) {
                    $data = Event::find($event -> id);
                    $data->status = 'Active';
                    $data->save();
                    // $event -> status = 'Active';
                }

                elseif($data_difference < 0) {
                    $data = Event::find($event -> id);
                    $data->status = 'Inactive';
                    $data->save();
                    // $event -> status = 'Inactive';
                }

                else {
                    $data = Event::find($event -> id);
                    $data->status = 'Active';
                    $data->save();
                    // $event -> status = 'Active';
                }
            }
            return view('events.list')->with('events', $events);
            

        }
        else{
            return redirect()->route('login');
        }
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'description'=>'required|min:1',
            'start_date'=>'required|after:yesterday',
            'end_date'=>'required|after:start_date',
            ]);

        $events = New Event;

        $events->description = $request -> description;
        $events->start_date = $request -> start_date;
        $events->end_date = $request -> end_date;
        $events->status = 'Active';

        $events->save();

        return redirect()->route('events.index')->with('message', 'Event added successfully');
    }

    public function show($id)
    {
        $events = Event::find($id);
        $candidates = Candidate::where('event_id', $id)->orderBy('position_id')->paginate(100);
        $students = Student::where('event_id', $id)->paginate(50, ['*'], 'studShow');
        $courses = Student::select('course')->distinct()->pluck('course', 'course')->toArray();
        $positions = Position::where('event_id', $id)->paginate(10, ['*'], 'candShow');
        $positions1 = Position::where('event_id', $id)->pluck('description', 'id');
        return view('events.eventdetails')->with('courses', $courses)->with('events', $events)->with('candidates', $candidates)->with('students', $students)->with('positions', $positions)->with('positions1', $positions1)->with(compact('studShow', 'candShow'));
    }   

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {

        $events = Event::find($request->input('id'));

        $events->description = $request->description;
        $events->start_date = $request->start_date;
        $events->end_date = $request->end_date;

        $events->save();
        return redirect()->route('events.index')->with('message', 'Event updated successfully');
    }

    public function destroy($id)
    {
        dd($id);
        $students = Student::where('event_id', $id)->destroy();
        return redirect()->route('events.show', $event_id)->with('message', 'Cleared Students List');
    }
}
