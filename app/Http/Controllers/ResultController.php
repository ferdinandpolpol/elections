<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Event;
use App\Student;
use App\Candidate;
use App\Position;
use App\Vote;
use App\VoteDetails;
use App\Result;
class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $candidates = Candidate::where('event_id', $id)->get();
        $max_pos_level = Position::where('event_id', $id)->max('position_level');
        $min_pos_level = Position::where('event_id', $id)->min('position_level');

        $positions = Position::select('id', 'description', 'position_level')->where('event_id', $id)->orderBy('id', 'desc')->get();

        $candidates_array = array();
        $votescount_array = array();
        for($current_level = $max_pos_level; $current_level >= $min_pos_level; $current_level--){
            $count = 0;
            foreach ($candidates as $candidate) {
                if($candidate->position->position_level == $current_level){
                    $candidates_array[$current_level][$count] = $candidate;

                    // $votescount_array[$candidate->id] = DB::select("select count(*) as count from(select vote_id, candidate_id, student_id from votedetails vd join votes v on v.id = vd.vote_id where vd.candidate_id = " . $candidate->id. " group by student_id) as c")[0]->count;
                    $votescount_array[$candidate->id] = DB::table('votedetails')->where('candidate_id', $candidate->id)->count(); 
                    $count++;
                }
            }
        }

        $total_voters = Student::where('has_voted', 1)->where('event_id', $id)->count();
        $total_stud = Student::where('event_id', $id)->count();
        return view('votes.result')->with('candidates_array', $candidates_array)->with('positions', $positions)->with('max_pos', $max_pos_level)->with('min_pos', $min_pos_level)->with('event_id', $id)->with('votescount_array', $votescount_array)->with('total_voters', $total_voters)->with('total_stud', $total_stud);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
