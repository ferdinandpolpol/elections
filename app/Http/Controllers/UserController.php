<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(100);
        $user_roles = DB::table('users_roles')
        ->join('users', 'users_roles.user_id', '=', 'users.id')
        ->join('roles', 'users_roles.role_id', '=', 'roles.id')
        ->get(['users.name AS username', 'roles.name as rolename','description', 'email', 'users.id as id', 'users.password as password']);

        $roles = Role::pluck('name', 'id');
        return view('users.index')->with('users', $user_roles)->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
        
        $user = User::create([
            'status' => 0,
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $user_role = User::find($user->id);
            $user_role->roles()->attach($request->role_id);

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $users = User::find($request->id);

        if($users->name == $request->name){
            $users->name = $request->name;
        }
        else {
            $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);
            $users->name = $request->name;
        }
        if($users->email == $request->email){
            $users->email = $request->email;
        }
        else {
            $this->validate($request, [
            'email' => 'required|string|email|max:255|unique:users',
        ]);
            $users->email = $request->email;
        }

        $users->roles()->detach();
        $users->roles()->attach($request->role_id);

        $users->save();
        return redirect()->route('users.index')->with('message', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('users')->where('id', $id)->delete();
        return redirect()->route('users.index')->with('message', 'User DELETED');
    }
}
