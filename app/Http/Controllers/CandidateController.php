<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Candidate;
use App\Event;
use Session;
use File;

class CandidateController extends Controller
{
    public function index()
    {
        $candidates = Candidate::paginate(10);
        return view('candidates.index')->withCandidate($candidates);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        /*$this->validate($request, [
            'lastname'=>'required',
            'firstname'=>'required',
            'position_id'=>'required',
            ]);*/

        $filename = "";
        if($request -> hasFile('image')){
            $filename = str_replace(' ', '', $request->lastname . $request->firstname.'.jpeg');
            $file = $request->image;
            $file->move('images', $filename);
        }
        $candidates = New Candidate;

        $candidates->img_url = "/images/" . $filename;
        $candidates->firstname = $request->firstname;
        $candidates->lastname = $request->lastname;
        $candidates->position_id = $request->position_id;
        $candidates->event_id = $request->event_id;


        $candidates->save();

        return redirect()->route('events.show', $request -> event_id)->with('message', 'Candidate added successfully');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
        $candidates = Candidate::find($id);
        return view('candidates.edit')->withCandidates($candidates);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'lastname'=>'required',
            'firstname'=>'required',
        ]);

        $filename = "";
        $image_path = "/images/" . $request -> lastname . $request -> firstname . '.jpeg';
        if($request -> hasFile('image')){
            $filename = str_replace(' ', '', $request -> lastname . $request -> firstname.'.jpeg');
            $file = $request -> image;
            $file->move('images', $filename);
        }

        $candidates = Candidate::find($request->id);
        $image = $candidates->img_url;

        $candidates->firstname = $request->firstname;
        $candidates->lastname = $request->lastname;
        $candidates->position_id = $request->position_id;
        if(is_null($request->image)){
            $candidates->img_url = $image;
        }
        else {
            if(File::exists($image_path)){
                File::delete($image_path);
            }
            $candidates->img_url = "/images/" . $filename;
        }
        
        $candidates->event_id = $request->event_id;

        $candidates->save();

        return redirect()->route('events.show', $request->event_id)->with('message', 'Candidate updated successfully');

    }

    public function destroy($id)
    {
        
        $candidate = Candidate::find($id);
        $event_id = $candidate->event_id;
        $candidate->delete();
        return redirect()->route('events.show', $event_id)->with('message', 'Candidate DELETED');

    }
}
