<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Position;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $contains = Position::where('event_id', $request -> event_id);
            $this->validate($request, [

            // 'position_level' => Rule::unique('position_level')->ignore($request -> event_id, $contains -> event_id),
            'position_level' => 'unique:positions,position_level,null,null,event_id,'. $request -> event_id,
            'description' => 'unique:positions,description,null,null,event_id,'. $request -> event_id,
            

            ]);
        
        $positions = New Position;

        $positions->description = $request -> description;
        $positions->position_level = $request -> position_level;
        $positions->is_multiple = $request -> is_multiple;
        $positions->multiple_limit = $request -> multiple_limit;
        $positions->limitation = $request -> limitation;
        $positions->event_id = $request -> event_id;



        $positions->save();

        return redirect()->route('events.show', $request -> event_id)->with('message', 'Position added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $old_value = Position::find($request -> id);

        if($old_value -> description == $request -> description) {
            $old_value->description = $request -> description;
        }
        else {
            $this->validate($request, [
                'description' => 'unique:positions,description,null,null,event_id,'. $request -> event_id,
            ]);
            $old_value->description = $request -> description;
        }
        if($old_value -> position_level == $request -> position_level){
            $old_value->position_level = $request -> position_level;
        }
        else {
            $this->validate($request, [
                'position_level' => 'unique:positions,position_level,null,null,event_id,'. $request -> event_id,
            ]);
            $old_value->position_level = $request -> position_level;
        }

        $old_value->event_id = $request -> event_id;

        $old_value->save();

        return redirect()->route('events.show', $request -> event_id)->with('message', 'Position added successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $position = Position::find($id);
        $event_id = $position->event_id;
        $position->delete();
        return redirect()->route('events.show', $event_id)->with('message', 'Position DELETED');
    }
}
