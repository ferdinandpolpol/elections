<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Event;
use App\Student;
use App\Candidate;
use App\Position;
use App\Vote;
use App\VoteDetails;

class VoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $student = Student::find($request->student_id);
        $events = Event::find($request->event_id);
        
        if($student->has_voted == 1){
            return view('votes.index')->with('events', $events);
        }
        else{

            $inputs = Input::get();
    
            $vote = new Vote;
            $vote->student_id = $request->student_id;
            $vote->event_id = $request->event_id;
            $vote->date = date('Y-m-d H:i:s');
    
            $vote->save();
    
            $last_vote = Vote::select('id')->max('id');
            
            $position_levels = Position::select('position_level')->where('event_id', $request->event_id)->get();
            $positions_with_multiple = Position::select()->where('event_id', $request->event_id)->where('is_multiple', '1')->pluck('position_level', 'multiple_limit');
            foreach($positions_with_multiple as $position_level => $multiple_limit){
                $counter = 0;
                if(isset($inputs['checkbox-' .  $position_level])){
                    foreach($inputs['checkbox-' .  $position_level] as $position_lvl => $candidate_level){
                        if($counter++ > $multiple_limit){
                            break;
                        }
                        if($candidate_level == 0){
                            continue;
                        }
                        else{
                            $vote_detail = new VoteDetails;
                            $vote_detail->vote_id = $last_vote;
                            $vote_detail->candidate_id = $candidate_level;
                            $vote_detail->save();
                            $counter++; 
                        }   
                    }
                }
            }
            if(isset($inputs['radiobutton'])){
                foreach($inputs['radiobutton'] as $position_level => $candidate_level){
                    if($candidate_level == 0){
                        continue;
                    }
                    else{
                        $vote_detail = new VoteDetails;
                        $vote_detail->vote_id = $last_vote;
                        $vote_detail->candidate_id = $candidate_level;
                        $vote_detail->save();
                    }
                }
            }
    
            $student->has_voted = 1;
            $student->save();
    
            return view('votes.index')->with('events', $events);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student_voter = null;
        $events = Event::find($id);
        return view('votes.index')->with('events', $events)->with('students', $student_voter);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ValidateStudent($id, Request $request){
        $student_voter = Student::where('event_id', $id)->where('student_code', $request->student_code)->first();
        $candidates = Candidate::where('event_id', $id)->get();
        $max_pos_level = Position::where('event_id', $id)->max('position_level');
        $min_pos_level = Position::where('event_id', $id)->min('position_level');

        $positions = Position::select('id', 'description', 'position_level')->where('event_id', $id)->orderBy('id', 'desc')->get();

        $candidates_array = array();

        if($student_voter == null){
            return redirect()->route('votes.show', $id)->with('message', 'Student does not exist in this event');
        }
        else if($student_voter->has_voted == 1){
            return redirect()->route('votes.show', $id)->with('message', 'Student has already voted!');
        }
        else
        {
            for($current_level = $max_pos_level; $current_level >= $min_pos_level; $current_level--){
                $count = 0;
                foreach ($candidates as $candidate) {
                    if($candidate->position->position_level == $current_level){
                        $candidates_array[$current_level][$count] = $candidate;
                        $count++;
                    }
                }
            }
            return view('votes.voting')->with('student_voter', $student_voter)->with('candidates_array', $candidates_array)->with('positions', $positions)->with('max_pos', $max_pos_level)->with('min_pos', $min_pos_level)->with('event_id', $id);
        }
    }

    public function StartVoting($id, Request $request){

    }
}