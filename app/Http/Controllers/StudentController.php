<?php

namespace App\Http\Controllers;

ini_set('max_execution_time', 0);

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use App\Student;
use DB;
use App\Event;
use App\Candidate;
use App\Position;
use Session;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = $request->id;

        if($request->search == ""){
            $events = Event::find($id);
            $candidates = Candidate::where('event_id', $id)->orderBy('position_id')->paginate(100);
            $students = Student::where('event_id', $id)->paginate(50, ['*'], 'studShow');
            $positions = Position::where('event_id', $id)->paginate(10, ['*'], 'candShow');
            $positions1 = Position::where('event_id', $id)->pluck('description', 'id');

            return view('events.eventdetails')->with('events', $events)->with('candidates', $candidates)->with('students', $students)->with('positions', $positions)->with('positions1', $positions1)->with(compact('studShow', 'candShow'));
        }
        else{
            $events = Event::find($id);
            $candidates = Candidate::where('event_id', $id)->orderBy('position_id')->paginate(100);
            $students = Student::where('event_id', $id)->where('firstname', 'LIKE', '%' . $request->search . '%')->orWhere('lastname', 'LIKE', '%' . $request->search . '%')->paginate(50, ['*'], 'studShow');
            $students->appends($request->only('search'));
            $positions = Position::where('event_id', $id)->paginate(10, ['*'], 'candShow');
            $positions1 = Position::where('event_id', $id)->pluck('description', 'id');

            return view('events.eventdetails')->with('events', $events)->with('candidates', $candidates)->with('students', $students)->with('positions', $positions)->with('positions1', $positions1)->with(compact('studShow', 'candShow'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Research data chunking to make insertion more faster
        // Another solution is make the overall Students List changeable rather than students per event.
        // https://laravel.io/forum/06-17-2016-how-do-i-use-chunk-to-insert-large-amounts-of-data-into-database
        $event_id = $request->event_id;
        Excel::load(Input::file('thefile'), function ($reader) use($event_id){
            foreach ($reader->toArray() as $row) {
                $students = new Student;
                $students->student_code = $row["code"];
                $students->firstname = $row["firstname"];
                $students->lastname = $row["lastname"];
                $students->course = $row["course"];
                $students->event_id = $event_id;
                $students->save();
            }
        });

        return redirect()->route('events.show', $event_id)->with('message', 'Students added succesfully');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_id)
    {
        DB::table('students')->where('event_id', $event_id)->delete();
        return redirect()->route('events.show', $event_id)->with('message', 'Student list DELETED');
    }

    public function clear_students($event_id){
    }
}
